<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package WP_Bootstrap_4
 */

get_header(); ?>

<div id="header-dark-section" class="dark-header-unit bg-dark-green"></div>

<div id="single-section" class="base-xs-single  single-unit" >
    <div class="screen-xs padding-bottom-40">
        <div class="container">
            <div class="row">
            	<?php
					if ( have_posts() ) : 
				?>

              <div class="col-md-8 col-12">
                  <div class="bred-crumb-wrap">
                    <?php echo do_shortcode( '[breadcrumb]' ); ?>
                  </div>

                  <div class="title-wrap top-banner-blurb" data-aos="fade-up" data-aos-duration="1500">
                         <?php
								the_archive_title( '<h2 class="page-title">', '</h2>' );
								the_archive_description( '<div class="archive-description text-muted">', '</div>' );
							?>
                      <div class="highlight"></div>                                      
                  </div>  
                  <div class="arg-uaa">
	                  	<?php
						/* Start the Loop */
						while ( have_posts() ) : the_post();

							/**
							 * Run the loop for the search to output the results.
							 * If you want to overload this in a child theme then include a file
							 * called content-search.php and that will be used instead.
							 */
							get_template_part( 'template-parts/content', 'search' );

						endwhile;

						the_posts_navigation( array(
							'next_text'         => esc_html__( 'Newer Posts', 'wp-bootstrap-4' ),
							'prev_text'         => esc_html__( 'Older Posts', 'wp-bootstrap-4' ),
						) );

					else :

						get_template_part( 'template-parts/content', 'none' );

					endif; ?>

                 </div>                
              </div>


              <div class="col-md-4 col-12 no-res-hide">
                  <div class="grey-strip single-gg-strip"></div>
                  <div class="letus-side leaa-side padding-bottom-40">   
                      <div class="rital-wrap text-center">
                          <div class="we-met">
                            <?php get_sidebar(); ?>
                          </div>
                      </div>
                  </div>
              </div>

            </div>
        </div>        
    </div>
</div>




<?php
get_footer();
