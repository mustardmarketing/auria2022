<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WP_Bootstrap_4
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

     <!-- AOS Animation -->
     <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
     <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>


    <!--Font Awesome -->
    <script src="https://use.fontawesome.com/cf3100053a.js"></script>

    <!--swiper slider -->
    <link rel="stylesheet"  href="<?php echo esc_url( home_url( '/' ) ); ?>wp-content/themes/auria/assets/swiper/swiper-bundle.min.css" />
    <script src="<?php echo esc_url( home_url( '/' ) ); ?>wp-content/themes/auria/assets/swiper/swiper-bundle.min.js"></script>

		<!--Google Font -->
		<link rel="preconnect" href="https://fonts.googleapis.com">
		<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
		<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400&display=swap" rel="stylesheet">

     
	<?php wp_head(); ?>
	
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">

<div id="mia-head" class="mia-head-wrap">
	<div id="masthead" class="site-header <?php if ( get_theme_mod( 'sticky_header', 0 ) ) : echo 'sticky-top'; endif; ?>">
		<div class="container">
              <div class="row">
                  <div class="col-md-3 col-6">
                  	<div class="custo-logo-wrap">
                  		<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
                  			<img src="<?php echo esc_url( home_url( '/' ) ); ?>wp-content/themes/auria/assets/images/Auria-Logo.svg" />
                  		</a>
                  	</div>
                  </div>
                  <div class="col-md-9 col-6">
	                    <div class="menu-wrap dark-themey">
		                    	<div class="anime-search-wrap">
		                    		<div class="top-infos">
		                    			<ul>
		                    				<li>
		                    					Contact Us : <a href="tel:0876548833">087 654 8833</a>
		                    				</li>
		                    				<li>|</li>
		                    				<li>
		                    					<a href="mailto:info@auria.co.za">info@auria.co.za</a>
		                    				</li>
		                    				<li>|</li>
		                    				<li>
		                    					<a href="https://www.facebook.com/auriaseniorliving/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
		                    				</li>
		                    			</ul>
		                    			
		                    		</div>
													 <?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
													
												 </div>						
									    </div>
											<div class="mobi-menu-wrap">
											      <div class="lat-wrapper">
											        <div id="lat"> 
											          <span class="menu-liner"></span>
											          <span class="menu-liner"></span>
											          <span class="menu-liner"></span>
											        </div>
											        <div class="menuy" id="menuy">
											          <?php wp_nav_menu( array( 'theme_location' => 'mobi-menu' ) ); ?> 
											          
											          <button id="close">✕</button>
											        </div>
											      </div>
											</div>

				   				</div>
              </div>
        </div>
	</div>
</div>

	<div id="content" class="site-content mely-top">
