<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WP_Bootstrap_4
 */

?>

<div id="post-<?php the_ID(); ?>" class="oniat">       
                  
                  
                  <div class="news-area-jin">

                      <div class="newsty-unit">
                       <div class="newsty-photo" style="background-image: url(<?php the_post_thumbnail_url(); ?>);">
                                
                                    <a href="<?php the_permalink(); ?>"></a>
                          </div>
                          <div class="newsty-blurb">
                              <div class="newsty-title">
                                <p><?php the_date(); ?></p>
                               <?php the_title( sprintf( '<h6 class="entry-title card-title"><a href="%s" rel="bookmark" class="text-dark">', esc_url( get_permalink() ) ), '</a></h6>' ); ?>
                              </div>
                              <div class="newsty-excerpt">
                                  <?php 
                                      $sumbuj = get_the_content(); 
                                      echo substr($sumbuj,0,112).'...';
                                  ?>    <br/>
                                  <a href="<?php the_permalink(); ?>">Read More</a>
                              </div>
                          </div>
                        </div>                     

                  </div>    

</div>
