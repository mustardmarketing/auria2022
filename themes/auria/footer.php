<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WP_Bootstrap_4
 */

?>

	</div><!-- #content -->

  <div id="footer-light" class="footer-unit footer-unit-light">
		<div class="container first-footer">
      <div class="row">  
        <div class="col-md-2 col-sm-6 col-12">
          <div class="footer-logo">
            <a href="">
              <img src="<?php echo esc_url( home_url( '/' ) ); ?>wp-content/themes/auria/assets/images/Auria-Logo.svg" />
            </a>
          </div>
        </div>
        <div class="col-md-3 col-sm-6 col-12">
          <div class="footling palia-one all-in">
            <div class="footling-title all-in">
              <span>ABOUT</span>
              <div class="highlight highlight-yellow highlight-left"></div>
            </div>
            <div class="footling-body all-in">
              <?php wp_nav_menu( array( 'theme_location' => 'footer-home-menu' ) ); ?>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-6 col-12">
          <div class="footling palia-two all-in">
            <div class="footling-title all-in">
              <span>LIVING OPTIONS</span>
              <div class="highlight highlight-yellow highlight-left"></div>
            </div>
            <div class="footling-body all-in">
               <?php wp_nav_menu( array( 'theme_location' => 'footer-terms-menu' ) ); ?>
            </div>
          </div>
        </div>
        <div class="col-md-2 col-sm-6 col-12">
          <div class="footling palia-three all-in">
            <div class="footling-title all-in">
              <span>COMMUNITIES</span>
              <div class="highlight highlight-yellow highlight-left"></div>
            </div>
            <div class="footling-body all-in">
              <?php wp_nav_menu( array( 'theme_location' => 'footer-community-menu' ) ); ?>
            </div>
          </div>
        </div>
        <div class="col-md-2 col-sm-6 col-12">
          <div class="footling all-in">
            <div class="footling-title all-in">
              <span>CONTACT INFO</span>
              <div class="highlight highlight-yellow highlight-left"></div>
            </div>
            <div class="footling-body all-in">
              <p>Suite 802, The Firestation<br/>
                  16 Baker Street, Rosebank
                </p>
                <ul>
                  <li><a href="tel:info@auria.co.za"><i class="fa fa-phone" aria-hidden="true"></i>087 654 8833</a></li>
                  <li><a href="mailto:info@auria.co.za"><i class="fa fa-envelope" aria-hidden="true"></i>info@auria.co.za</a></li>
                  <li><a href="https://www.facebook.com/auriaseniorliving/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i>Facebook</a></li>
                </ul>
              
            </div>
          </div>
        </div>
      </div>
    </div>   
    <div class="container second-footer">
      <div class="row">  
        <div class="col-md-1 col-12">
          <div class="me-join"><strong>JOIN US</strong></div>
        </div>
        <div class="col-md-11 col-12">
          <div class="footer-newsletter">
            <?php gravity_form(2, false, false, false, '', true, 12); ?> 
          </div>
        </div>
      </div>
    </div> 
    <div class="container third-footer">
      <div class="row">  
        <div class="col-md-6 col-12">
          <div class="copyrite">
            Copyright 2021 | AURIA SENIOR LIVING | All Rights Reserved | POPIA Manual | PAIA Manual
          </div>
        </div>
        <div class="col-md-6 col-12">
          <div class="copyrite text-right">
            <a href="" target="_blank">Design by Mustard</a>
          </div>
        </div>
      </div>
    </div>   
  </div><!-- #footer -->


</div><!-- #page -->






  <!-- Demo styles -->
  <style>

    .swiper-button-next, .swiper-container-rtl .swiper-button-prev, .swiper-button-prev {
        outline: none !important;
    }

    .swiper-container {
      width: 100%;
      height: 100%;
    }
    .swiper-slide {
      background: #fff;

    }
</style>


<script>
  AOS.init();
</script>



<script>

    jQuery(document).ready(function() {


      // ===== Sticky header ==== 
        jQuery(window).scroll(function() {
          if (jQuery(this).scrollTop() > 150){  
              jQuery('#mia-head').addClass("sticky");
          }
          else{
              jQuery('#mia-head').removeClass("sticky");
          }
      });
      
      // Mobi menu script 
      document.querySelector("#lat").addEventListener("click", () => {
        menuy.classList.toggle("open");
      });

      document.querySelector("#close").addEventListener("click", () => {
        menuy.classList.toggle("open");
      });
      
  });

  </script>




<?php wp_footer(); ?>

</body>
</html>
