<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WP_Bootstrap_4
 */

get_header(); ?>

<div id="header-dark-section" class="dark-header-unit bg-dark-green"></div>

<div id="single-section" class="base-xs-single  single-unit" >
    <div class="screen-xs padding-bottom-40">
        <div class="container">
            <div class="row">
            	<?php
					if ( have_posts() ) : 
				?>

              <div class="col-md-8 col-12 padding-top-20">

                  <div class="title-wrap top-banner-blurb" data-aos="fade-up" data-aos-duration="1500">
                         <?php
								the_archive_title( '<h2 class="page-title">', '</h2>' );
								the_archive_description( '<div class="archive-description text-muted">', '</div>' );
							?>
                      <div class="highlight"></div>                                      
                  </div>  
                  <div class="arg-uaa">
	                  	<?php
							/* Start the Loop */
							while ( have_posts() ) : the_post();

								/*
								 * Include the Post-Format-specific template for the content.
								 * If you want to override this in a child theme, then include a file
								 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
								 */
								get_template_part( 'template-parts/content', get_post_format() );

							endwhile;

							the_posts_navigation( array(
								'next_text' => esc_html__( 'Newer Posts', 'wp-bootstrap-4' ),
								'prev_text' => esc_html__( 'Older Posts', 'wp-bootstrap-4' ),
							) );

						else :

							get_template_part( 'template-parts/content', 'none' );

						endif; ?>
                 </div>                
              </div>




             <div class="col-md-4 col-12 galaap">

                  <div class="widget-arealy">

                    <div class="widget-area-sub all-in">
                      <div class="chaser grey-bg"></div>
                      <div class="widget-area-content">
                        <div class="title-wrap top-banner-blurb main-intro body-intro" data-aos="fade-up" data-aos-duration="1500">
                            <h3>
                              NEWSLETTERS
                            </h3>
                            <div class="highlight"></div>
                        </div>
                      </div>
                    </div>
                    <div class="widget-area-sub all-in">
                      <div class="chaser green-bg"></div>
                      <div class="widget-area-content text-white">
                        <div class="title-wrap top-banner-blurb main-intro body-intro" data-aos="fade-up" data-aos-duration="1500">
                            <h3>
                              USEFUL DOWNLOADS
                            </h3>
                            <div class="highlight"></div>
                        </div>
                      </div>
                    </div>

                  </div>
              </div>

            </div>
        </div>        
    </div>
</div>




<?php
get_footer();
