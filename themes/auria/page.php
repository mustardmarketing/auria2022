<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WP_Bootstrap_4
 */

get_header(); ?>



<?php if (have_posts()) : while (have_posts()) : the_post(); ?>  


<div id="header-dark-section" class="dark-header-unit bg-dark-green"></div>


<div id="poliy" class="base-xs-pagey  pagey-unit" >
    <div class="screen-xs padding-bottom-40">
        <div class="container">
            <div class="row">
              <div class="col-12 padding-top-20">

                  <div class="title-wrap top-banner-blurb" data-aos="fade-up" data-aos-duration="1500">
                      <h3>
                          <?php the_title(); ?>
                      </h3>
                      <div class="highlight"></div>                                      
                  </div>  
                  <div class="ply-bm-wrapper all-in">
                        <?php the_content(); ?>
                  </div>                 
              </div>

            </div>
        </div>        
    </div>
</div>


<?php endwhile; ?>
<?php endif; ?>


<?php
get_footer();
