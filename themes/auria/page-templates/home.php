<?php
/*
* Template Name: Homepage
*/

get_header(); ?>

<?php
 // $arges = array( 'post_type' => 'page', 'p'=>'2', 'posts_per_page' => 1,);
 // $related_props = new WP_Query( $arges ); while ( $related_props->have_posts() ) : $related_props->the_post();
?>



<div id="home-section" class="base-xs-home home-section" style="background-image: url(http://mustard.studio/auria2019/wp-content/uploads/2019/08/testhead.jpg);">
    <div class="screen-xs padding-80">
        <div class="container">
          <div class="long-yellow-line" data-aos="fade-down" data-aos-duration="1500"></div>
            <div class="row">
              <div class="col-12"> 


                <div class="title-wrap top-banner-blurb text-center text-white main-intro" data-aos="fade-up" data-aos-duration="1500">
                    <p class="top-sub-tite text-white">
                      WELCOME TO AURIA SENIOR LIVING
                    </p>
                    <div class="highlight highlight-center highlight-yellow"></div>
                    <h1>
                      Luxury Living for over 70’s
                    </h1>
                    
                    <div class="hmm-btn all-in">
                      <a href="#" class="btn-side white-btn" data-toggle="modal" data-target="#mgi-disclaimer">THE AURIA WAY  &nbsp; <i class="fa fa-angle-right" aria-hidden="true"></i> </a>
                    </div>
                </div>                  
              </div>
            </div>
        </div>        
    </div>
</div>

<div id="home-intro-section" class="base-xs-home home-intro-unit">
  <div class="screen-xs padding-40">
      <div class="container">
          <div class="row">
             <div class="col-12 padding-40">
                  <div class="title-wrap concise text-center">
                    <h2>Complete Peace Of Mind For Your Later Years</h2>
                    <div class="highlight highlight-center highlight-yellow"></div>
                    <p>
                      At Auria Senior Living we believe that your later years should be defined by choice, and not by compromise. Our beautifully designed, centrally located, purpose-built communities provide the ideal setting for residents to enjoy full and enriching lives with the daily support and continuum of care that provides complete peace of mind.
                    </p>
                  </div>    
             </div>
          </div>
      </div>
    </div>
</div>

<div id="home-news-section" class="baso-xs-home home-news-unit">
  <div class="screen-xs padding-bottom-80">
      <div class="container">
          <div class="row">
             <div class="col-12 padding-40">
                  <div class="title-wrap concise text-center">
                    <h1>Latest News</h1>
                    <div class="highlight highlight-center highlight-yellow"></div>
                  </div> 
             </div>
          </div>
      </div>
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="swiper swiper-news">
              <!-- Additional required wrapper -->
              <div class="swiper-wrapper">


                <!-- Slides -->
                <div class="swiper-slide">
                  <div class="newslin-wrap">
                     <div class="newslin newslin-left">
                         <div class="bondi-left">
                            <div class="title-wrap">
                              <span>NEW DEVELOPMENT</span>
                              <h2>Coral Cove</h2>
                              <div class="highlight highlight-left highlight-yellow"></div>
                              <p>
                                Introducing a wave of new living at Coral Cove - centrally located, purpose-built senior living community on the magnificent North Coast of KwaZulu-Natal.
                              </p>
                            </div>    
                            <div class="home-btns">
                              <a href="#" class="btn-side">LEARN MORE &nbsp; <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                              <a href="#" class="btn-side transp-btn dark-btn" data-toggle="modal" data-target="#mgi-disclaimer">PRESS RELEASE &nbsp; <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                            </div> 
                          </div> 
                      </div>
                      <div class="newslin newslin-right">
                        <div class="newslin-photo" style="background-image: url(http://mustard.studio/auria2019/wp-content/uploads/2019/08/003.jpg);">
                          
                        </div>
                      </div>
                  </div>
                </div>

                <div class="swiper-slide">
                  <div class="newslin-wrap">
                     <div class="newslin newslin-left">
                         <div class="bondi-left">
                            <div class="title-wrap">
                              <span>NEW DEVELOPMENT</span>
                              <h2>Coral Cove Two</h2>
                              <div class="highlight highlight-left highlight-yellow"></div>
                              <p>
                                Introducing a wave of new living at Coral Cove - centrally located, purpose-built senior living community on the magnificent North Coast of KwaZulu-Natal.
                              </p>
                            </div>    
                            <div class="home-btns">
                              <a href="#" class="btn-side">LEARN MORE &nbsp; <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                              <a href="#" class="btn-side transp-btn dark-btn" data-toggle="modal" data-target="#mgi-disclaimer">PRESS RELEASE &nbsp; <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                            </div> 
                          </div> 
                      </div>
                      <div class="newslin newslin-right">
                        <div class="newslin-photo" style="background-image: url(http://mustard.studio/auria2019/wp-content/uploads/2019/07/7197.jpg);">
                          
                        </div>
                      </div>
                  </div>
                </div>
                <div class="swiper-slide">
                  <div class="newslin-wrap">
                     <div class="newslin newslin-left">
                         <div class="bondi-left">
                            <div class="title-wrap">
                              <span>NEW DEVELOPMENT</span>
                              <h2>Coral Cove Three</h2>
                              <div class="highlight highlight-left highlight-yellow"></div>
                              <p>
                                Introducing a wave of new living at Coral Cove - centrally located, purpose-built senior living community on the magnificent North Coast of KwaZulu-Natal.
                              </p>
                            </div>    
                            <div class="home-btns">
                              <a href="#" class="btn-side">LEARN MORE &nbsp; <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                              <a href="#" class="btn-side transp-btn dark-btn" data-toggle="modal" data-target="#mgi-disclaimer">PRESS RELEASE &nbsp; <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                            </div> 
                          </div> 
                      </div>
                      <div class="newslin newslin-right">
                        <div class="newslin-photo" style="background-image: url(http://mustard.studio/auria2019/wp-content/uploads/2019/08/005.jpg);">
                          
                        </div>
                      </div>
                  </div>
                </div>


                
              </div>

              <!-- If we need navigation buttons -->
              <div class="swiper-button-prev"></div>
              <div class="swiper-button-next"></div>

            </div>
           
          </div>         
        </div>
      </div>
    </div>
</div>

<div id="home-community" class="baso-xs-home bg-grey home-community">
  <div class="screen-xs padding-80">
      <div class="container">
        <div class="row">

          <div class="col-md-4 col-12">
            <div class="condi-left">
              <div class="title-wrap">
                <h2>Our<br/> Communities</h2>
                <div class="highlight highlight-left highlight-yellow"></div>
                <p>
                  Our beautifully designed, centrally located, purpose-built communities provide the ideal setting for residents to enjoy full and enriching lives with the daily support and continuum of care that provides complete peace of mind.
                </p>
              </div>  
            </div>             
          </div>
          <div class="col-md-8 col-12">
            <div class="swiper swiper-community">
              <!-- Additional required wrapper -->
              <div class="swiper-wrapper">

                <!-- Slides -->
                <div class="swiper-slide">
                  <div class="newslin-wrap">
                      <div class="commlin">
                        <div class="commlin-photo" style="background-image: url(http://mustard.studio/auria2019/wp-content/uploads/2019/08/003.jpg);">                          
                        </div>
                        <div class="commlin-logo all-in">
                          <img src="https://mustard.studio/aurias/wp-content/themes/auria/assets/images/mel.png" />
                        </div>
                        <div class="commlin-blurb all-in">
                          <p>The first things that strike you about San Sereno are its quiet sophistication and unsurpassed hospitality.</p>
                        </div>
                        <div class="commlin-btn all-in">
                          <a href="#" class="btn-side">LEARN MORE &nbsp; <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                      </div>
                  </div>
                </div>

                 <!-- Slides -->
                <div class="swiper-slide">
                  <div class="newslin-wrap">
                      <div class="commlin">
                        <div class="commlin-photo" style="background-image: url(http://mustard.studio/auria2019/wp-content/uploads/2019/07/7197.jpg);">                          
                        </div>
                        <div class="commlin-logo all-in">
                          <img src="https://mustard.studio/aurias/wp-content/themes/auria/assets/images/san.png" />
                        </div>
                        <div class="commlin-blurb all-in">
                          <p>The first things that strike you about San Sereno are its quiet sophistication and unsurpassed hospitality.</p>
                        </div>
                        <div class="commlin-btn all-in">
                          <a href="#" class="btn-side">LEARN MORE &nbsp; <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                      </div>
                  </div>
                </div>
                 <!-- Slides -->
                <div class="swiper-slide">
                  <div class="newslin-wrap">
                      <div class="commlin">
                        <div class="commlin-photo" style="background-image: url(http://mustard.studio/auria2019/wp-content/uploads/2019/08/003.jpg);">                          
                        </div>
                        <div class="commlin-logo all-in">
                          <img src="https://mustard.studio/aurias/wp-content/themes/auria/assets/images/roy.png" />
                        </div>
                        <div class="commlin-blurb all-in">
                          <p>The first things that strike you about San Sereno are its quiet sophistication and unsurpassed hospitality.</p>
                        </div>
                        <div class="commlin-btn all-in">
                          <a href="#" class="btn-side">LEARN MORE &nbsp; <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                      </div>
                  </div>
                </div>

              
              </div>

              <!-- If we need navigation buttons -->
              <div class="swiper-button-prev"></div>
              <div class="swiper-button-next"></div>

            </div>
            
          </div>

         
        </div>
      </div>
    </div>
</div>


<div id="home-testimonial" class="baso-xs-home bg-grey home-testimonial">
  <div class="screen-xs padding-80">
      <div class="container">
        <div class="row">

          <div class="col-md-8 col-12">
            <div class="swiper swiper-testimonial">
              <!-- Additional required wrapper -->
              <div class="swiper-wrapper">

                <!-- Slides -->
                <div class="swiper-slide">
                  <div class="testlin-wrap">
                      <div class="testlin-photo-wrap">
                        <div class="testlin-photo" style="background-image: url(http://mustard.studio/auria2019/wp-content/uploads/2019/08/003.jpg);">                          
                        </div>                      
                      </div>                      
                      <div class="testlin-blurb all-in">
                         <div class="testlin-test">
                           <p>“Living at San Sereno is absolutely fabulous - I just love the sense of community and the social activities.”</p>
                         </div>   
                         <div class="testlin-names">
                           Betty Jenkins
                         </div> 
                      </div>
                  </div>
                </div>

                 <!-- Slides -->
                <div class="swiper-slide">
                  <div class="newslin-wrap">
                      <div class="commlin">
                        <div class="commlin-photo" style="background-image: url(http://mustard.studio/auria2019/wp-content/uploads/2019/07/7197.jpg);">                          
                        </div>
                        <div class="commlin-logo all-in">
                          <img src="https://mustard.studio/aurias/wp-content/themes/auria/assets/images/san.png" />
                        </div>
                        <div class="commlin-blurb all-in">
                          <p>The first things that strike you about San Sereno are its quiet sophistication and unsurpassed hospitality.</p>
                        </div>
                        <div class="commlin-btn all-in">
                          <a href="#" class="btn-side">LEARN MORE &nbsp; <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                      </div>
                  </div>
                </div>
                 <!-- Slides -->
                <div class="swiper-slide">
                  <div class="newslin-wrap">
                      <div class="commlin">
                        <div class="commlin-photo" style="background-image: url(http://mustard.studio/auria2019/wp-content/uploads/2019/08/003.jpg);">                          
                        </div>
                        <div class="commlin-logo all-in">
                          <img src="https://mustard.studio/aurias/wp-content/themes/auria/assets/images/roy.png" />
                        </div>
                        <div class="commlin-blurb all-in">
                          <p>The first things that strike you about San Sereno are its quiet sophistication and unsurpassed hospitality.</p>
                        </div>
                        <div class="commlin-btn all-in">
                          <a href="#" class="btn-side">LEARN MORE &nbsp; <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                      </div>
                  </div>
                </div>

              
              </div>

              <!-- If we need navigation buttons -->
              <div class="swiper-button-prev"></div>
              <div class="swiper-button-next"></div>

            </div>
            
          </div>

         
        </div>
      </div>
    </div>
</div>


<div id="home-gallery" class="baso-xs-home home-gallery">
  <div class="screen-xs padding-80">
    <div class="container">
          <div class="row">
             <div class="col-12 padding-40">
                  <div class="title-wrap concise text-center">
                    <h1>Gallery</h1>
                    <div class="highlight highlight-center highlight-yellow"></div>
                  </div> 
             </div>
          </div>
      </div>
      <div class="container">
        <div class="row">          
          <div class="col-12">
            <div class="swiper swiper-gallery">
              <!-- Additional required wrapper -->
              <div class="swiper-wrapper">

                <!-- Slides -->
                <div class="swiper-slide">
                  <div class="newslin-wrap">
                      <div class="commlin">
                        <div class="commlin-photo" style="background-image: url(http://mustard.studio/auria2019/wp-content/uploads/2019/08/003.jpg);">                          
                        </div>
                      </div>
                  </div>
                </div>
                <!-- Slides -->
                <div class="swiper-slide">
                  <div class="newslin-wrap">
                      <div class="commlin">
                        <div class="commlin-photo" style="background-image: url(http://mustard.studio/auria2019/wp-content/uploads/2019/08/003.jpg);">                          
                        </div>
                      </div>
                  </div>
                </div>
                <!-- Slides -->
                <div class="swiper-slide">
                  <div class="newslin-wrap">
                      <div class="commlin">
                        <div class="commlin-photo" style="background-image: url(http://mustard.studio/auria2019/wp-content/uploads/2019/08/003.jpg);">                          
                        </div>
                      </div>
                  </div>
                </div>                 
                      
              </div>

              <!-- If we need navigation buttons -->
              <div class="swiper-button-prev"></div>
              <div class="swiper-button-next"></div>

            </div>
            
          </div>

         
        </div>
      </div>
    </div>
</div>


<div id="home-options" class="baso-xs-home bg-grey home-options" style="background-image: url(https://mustard.studio/aurias/wp-content/uploads/2021/11/two-strip-scaled.jpg);">
  <div class="screen-xs padding-80">
      <div class="container">
        <div class="row">
          <div class="col-md-4 col-12">
            <div class="condi-left">
              <div class="title-wrap">
                <h2>Living Options</h2>
                <div class="highlight highlight-left highlight-yellow"></div>
                <p>
                  As a continuing care retirement community (“CCRC”) we offer fully independent living, as well as various types of assisted living in one place, ranging from home-based care to supportive care (frail care) and dedicated dementia care.
                </p>
              </div>  
            </div>             
          </div>
          <div class="col-md-8 col-12">
            <div class="blurb-wrap padding-bottom-40">
              <h4>Independent Living</h4>
              <p>
                  Independent living is for people who are completely self-sufficient but want access to lifestyle amenities and assistance when necessary. access to lifestyle amenities and assistance when necessary.
              </p>
              <div class="home-btns">
                <a href="#" class="btn-side">READ MORE &nbsp; <i class="fa fa-angle-right" aria-hidden="true"></i></a>
              </div>
            </div>
            <div class="blurb-wrap padding-bottom-40">
              <h4>Assisted Living</h4>
              <p>
                  The support implied in assisted living ranges from almost complete independence to needing assistance with some or even many of what are termed ‘activities of daily living’ (ADLs).
              </p>
              <div class="home-btns">
                <a href="#" class="btn-side">READ MORE &nbsp; <i class="fa fa-angle-right" aria-hidden="true"></i></a>
              </div>
            </div>
          </div>
         
        </div>
      </div>
    </div>
</div>

<div id="home-award" class="baso-xs-home bg-yellow home-award">
  <div class="screen-xs padding-80">
      <div class="container">
        <div class="row">
          <div class="col-md-4 col-12">
            <div class="condi-left">
              <div class="title-wrap">
                <h2>Living Options</h2>
                <div class="highlight highlight-left highlight-yellow"></div>
                <p>
                  As a continuing care retirement community (“CCRC”) we offer fully independent living, as well as various types of assisted living in one place, ranging from home-based care to supportive care (frail care) and dedicated dementia care.
                </p>
              </div>  
            </div>             
          </div>
          <div class="col-md-8 col-12">
            <div class="blurb-wrap padding-bottom-40">
              <h4>Independent Living</h4>
              <p>
                  Independent living is for people who are completely self-sufficient but want access to lifestyle amenities and assistance when necessary. access to lifestyle amenities and assistance when necessary.
              </p>
              <div class="home-btns">
                <a href="#" class="btn-side">READ MORE &nbsp; <i class="fa fa-angle-right" aria-hidden="true"></i></a>
              </div>
            </div>
            <div class="blurb-wrap padding-bottom-40">
              <h4>Assisted Living</h4>
              <p>
                  The support implied in assisted living ranges from almost complete independence to needing assistance with some or even many of what are termed ‘activities of daily living’ (ADLs).
              </p>
              <div class="home-btns">
                <a href="#" class="btn-side">READ MORE &nbsp; <i class="fa fa-angle-right" aria-hidden="true"></i></a>
              </div>
            </div>
          </div>
         
        </div>
      </div>
    </div>
</div>




<?php// endwhile; ?>
<?php //wp_reset_query(); ?>

<script>

    jQuery(document).ready(function() {


      // News Swiper slider
      var swiper = new Swiper('.swiper-news', {
          slidesPerView: 1,
          spaceBetween: 0,
          loop: true,
          speed: 1500,
          fadeEffect: { crossFade: true },
          effect: "fade",
          // init: false,
          autoplay: {
            delay: 5000,
            disableOnInteraction: true,
          },
          navigation: {
              nextEl: ".swiper-button-next",
              prevEl: ".swiper-button-prev",
            },
          breakpoints: {
            640: {
              slidesPerView: 1,
              spaceBetween: 0,
            },
          }
        });

      // Community Swiper slider
      var swipeer = new Swiper('.swiper-community', {
         slidesPerView: 2,
          spaceBetween: 8,
          loop: true,
          speed: 1500,
          // init: false,
          autoplay: {
            delay: 6500,
            disableOnInteraction: false,
          },
           navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
          },
          breakpoints: {
            640: {
              slidesPerView: 1,
              spaceBetween: 5,
            },
            768: {
              slidesPerView: 2,
              spaceBetween: 8,
            },
            1024: {
              slidesPerView: 2,
              spaceBetween: 8,
            },
          }
        });

       // Testimonial Swiper slider
      var swipeor = new Swiper('.swiper-testimonial', {
         slidesPerView: 2,
          spaceBetween: 8,
          loop: true,
          speed: 1500,
          // init: false,
          autoplay: {
            delay: 6500,
            disableOnInteraction: false,
          },
           navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
          },
          breakpoints: {
            640: {
              slidesPerView: 1,
              spaceBetween: 5,
            },
            768: {
              slidesPerView: 2,
              spaceBetween: 8,
            },
            1024: {
              slidesPerView: 2,
              spaceBetween: 8,
            },
          }
        });

      // gallery Swiper slider
      var gallery = new Swiper('.swiper-gallery', {
        slidesPerView: 3,
          effect: "coverflow",
          grabCursor: true,
          centeredSlides: true,
          slidesPerView: "auto",
          spaceBetween: 2,
          loop: true,
          speed: 1500,
          coverflowEffect: {
            rotate: 50,
            stretch: 0,
            depth: 100,
            modifier: 1,
            slideShadows: true,
          },
          autoplay: {
            delay: 4000,
            disableOnInteraction: false,
          },
          navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
          },
          breakpoints: {
            640: {
              slidesPerView: 1,
              spaceBetween: 5,
            },
            768: {
              slidesPerView: 2,
              spaceBetween: 8,
            },
            1024: {
              slidesPerView: 3,
              spaceBetween: 8,
            },
          }
        });

    });

  </script>


<?php
get_footer();
