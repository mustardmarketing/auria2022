<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WP_Bootstrap_4
 */

get_header(); ?>


<?php if (have_posts()) : while (have_posts()) : the_post(); ?>  


<div id="header-dark-section" class="dark-header-unit bg-dark-green"></div>


<div id="single-section" class="base-xs-single  single-unit" >
    <div class="screen-xs padding-bottom-40">
        <div class="container">
            <div class="row">
              <div class="col-md-8 col-12 padding-top-20">

                  <div class="title-wrap top-banner-blurb" data-aos="fade-up" data-aos-duration="1500">
                      <p class="text-green">
                        <?php the_date(); ?>
                      </p>
                      <h3>
                          <?php the_title(); ?>
                      </h3>
                      <div class="highlight"></div>                                      
                  </div>  
                  <div class="why-bm-wrapper conty-lend">
                        <?php the_content(); ?>
                  </div>                 
              </div>
              
              <?php endwhile; ?>
              <?php endif; ?>

              <div class="col-md-4 col-12 galaap">

                  <div class="widget-arealy">

                    <div class="widget-area-sub all-in">
                      <div class="chaser grey-bg"></div>
                      <div class="widget-area-content">
                        <div class="title-wrap top-banner-blurb main-intro body-intro" data-aos="fade-up" data-aos-duration="1500">
                            <h3>
                              NEWSLETTERS
                            </h3>
                            <div class="highlight"></div>
                        </div>
                      </div>
                    </div>
                    <div class="widget-area-sub all-in">
                      <div class="chaser green-bg"></div>
                      <div class="widget-area-content text-white">
                        <div class="title-wrap top-banner-blurb main-intro body-intro" data-aos="fade-up" data-aos-duration="1500">
                            <h3>
                              USEFUL DOWNLOADS
                            </h3>
                            <div class="highlight"></div>
                        </div>
                      </div>
                    </div>

                  </div>
              </div>

            </div>
        </div>        
    </div>
</div>







<?php
get_footer();
